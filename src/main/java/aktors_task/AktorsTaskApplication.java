package aktors_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AktorsTaskApplication {


	public static void main(String[] args) {
		SpringApplication.run(AktorsTaskApplication.class, args);
	}
}
