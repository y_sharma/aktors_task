package aktors_task.controller;

import aktors_task.dao.ClientRepository;
import aktors_task.model.Client;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class ClientController {

    @Resource
    private ClientRepository clientRepository;


    @GetMapping("clients")
    public Iterable<Client> clients() {
        return clientRepository.findAll();
    }

    @GetMapping("client/{securityNr}")
    public Client client(@PathVariable Long securityNr) {
        return clientRepository.findBysecurityNr(securityNr);
    }

    @PostMapping("clients")
    public void saveClient(@RequestBody Client client) {
        clientRepository.save(client);
    }

    @DeleteMapping("clients/{securityNr}")
    public void deleteClient(@PathVariable Long securityNr) {
        clientRepository.delete(securityNr);
    }
}
