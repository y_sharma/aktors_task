package aktors_task.controller;


import aktors_task.dao.OrderRepository;

import aktors_task.model.Client;
import aktors_task.model.Order;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class OrderController {

    @Resource
    private OrderRepository orderRepository;

    @GetMapping("orders")
    public Iterable<Order> orders() {
        return orderRepository.findAll();
    }

    @GetMapping("order/{orderNr}")
    public Order order(@PathVariable Long orderNr) {
        return orderRepository.findByorderNr(orderNr);
    }

    @PostMapping("orders")
    public void saveOrder(@RequestBody Order order) {
        orderRepository.save(order);
    }

    @DeleteMapping("orders/{orderNr}")
    public void deleteOrder(@PathVariable Long orderNr) {
        orderRepository.delete(orderNr);
    }


}
