package aktors_task.controller;


import aktors_task.dao.ProductRepository;
import aktors_task.model.Product;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class ProductController {

    @Resource
    private ProductRepository productRepository;

    @GetMapping("products")
    public Iterable<Product> products(){
        return productRepository.findAll();
    }

    @GetMapping("product/{barcode}")
    public Product product(@PathVariable Long barcode) {
        return productRepository.findBybarcode(barcode);
    }

    @GetMapping("productByBarcode")
    public Product productByBarcode(@RequestParam Long barcode){
        return productRepository.findOne(barcode);
    }

    @PostMapping("products")
    public void saveProduct(@RequestBody Product product){
        productRepository.save(product);
    }

    @DeleteMapping("products/{barcode}")
    public void deleteProduct(@PathVariable Long barcode){
        productRepository.delete(barcode);
    }
}
