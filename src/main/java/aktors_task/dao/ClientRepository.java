package aktors_task.dao;

import aktors_task.model.Client;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

// http://javabeat.net/spring-data-jpa/
public interface ClientRepository extends CrudRepository<Client, Long> {

    Client findBysecurityNr(Long securityNr);

}
