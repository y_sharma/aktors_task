package aktors_task.dao;


import aktors_task.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long>{

    Order findByorderNr(Long orderNr);

}
