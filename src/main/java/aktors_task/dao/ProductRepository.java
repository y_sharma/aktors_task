package aktors_task.dao;

import aktors_task.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long>{

    Product findBybarcode(Long barcode);

}
