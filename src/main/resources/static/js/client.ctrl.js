(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ClientCtrl', Ctrl);

    function Ctrl($http, $routeParams, readonly) {

        var vm = this;
        vm.clients = [];
        vm.securityNr = '';
        vm.firstName = '';
        vm.lastName = '';
        vm.phone = '';
        vm.country = '';
        vm.address = '';
        vm.propertyName = 'firstName';
        vm.readonly = readonly;

        vm.addNewClient = addNewClient;
        vm.deleteClient = deleteClient;

        init();

        function init() {
            //if client securityNr is defined
            if ($routeParams.id !== undefined) {
                $http.get('http://localhost:8080/client/' + $routeParams.id).then(function (result) {
                    //console.log('aa: ' + result.data.securityNr);
                    vm.securityNr = result.data.securityNr;
                    vm.firstName = result.data.firstName;
                    vm.lastName = result.data.lastName;
                    vm.phone = result.data.phone;
                    vm.country = result.data.country;
                    vm.address = result.data.address;
                });
            }
            // if client securityNr is not defined
            $http.get('http://localhost:8080/clients').then(function (result) {
                //console.log('aa: ' + JSON.stringify(result));
                vm.clients = result.data;
            });
        }


        function addNewClient() {
            var newClient = {
                securityNr: vm.securityNr,
                firstName: vm.firstName,
                lastName: vm.lastName,
                phone: vm.phone,
                country: vm.country,
                address: vm.address
            };
            $http.post('http://localhost:8080/clients', newClient).then(function () {
                init();
            });
        }

        function deleteClient(securityNr) {
            $http.delete('http://localhost:8080/clients/' + securityNr).then(function () {
                init();
            });
        }

    }

})();
