(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('OrderCtrl', Ctrl);

    function Ctrl($http, $routeParams, readonly) {

        var vm = this;
        vm.orders = [];
        vm.basePrice = '';
        vm.price = 0;
        vm.propertyName = 'transactionDate';
        vm.readonly = readonly;

        // all clients
        vm.clients = [];
        // 1 client chosen from all clients
        vm.client = '';

        // all products
        vm.allProducts = [];
        // 1 product chosen from all products
        vm.product = '';
        // chosen product is added here
        vm.products = [];

        vm.addNewOrder = addNewOrder;
        vm.deleteOrder = deleteOrder;
        vm.addProduct = addProduct;

        init();
        getClients();
        getProducts();

        function addProduct() {
            // console.log('1 prod: ' + JSON.stringify(vm.product));
            // console.log('1 prod price: ' + JSON.stringify(vm.product.basePrice));
            //console.log('price: ' + vm.price);

            vm.price += vm.product.basePrice;
            vm.products.push(vm.product);
        }

        function getClients() {
            $http.get('http://localhost:8080/clients').then(function (result) {
                //console.log('aa: ' + JSON.stringify(result));
                vm.clients = result.data;
            });
        }

        function getProducts() {
            $http.get('http://localhost:8080/products').then(function (result) {
                //console.log('aa: ' + JSON.stringify(result));
                vm.allProducts = result.data;
            });
        }


        function init() {
            //if order securityNr is defined
            if ($routeParams.id !== undefined) {
                $http.get('http://localhost:8080/order/' + $routeParams.id).then(function (result) {
                    //console.log('aa: ' + result.data.securityNr);
                    vm.orderNr = result.data.orderNr;
                    vm.basePrice = result.data.basePrice;
                    vm.products = result.data.products;
                });
            }
            // if order securityNr is not defined
            $http.get('http://localhost:8080/orders').then(function (result) {
                //console.log('aa: ' + JSON.stringify(result));
                vm.orders = result.data;
            });
        }

        function addNewOrder() {
            var newOrder = {
                price: vm.price,
                transactionDate: new Date(),

                client: vm.client,
                products: vm.products
            };
            vm.products = [];
            vm.price = 0;
            $http.post('http://localhost:8080/orders', newOrder).then(function () {
                init();
            });
        }

        function deleteOrder(orderNr) {
            $http.delete('http://localhost:8080/orders/' + orderNr).then(function () {
                init();
            });
        }

    }

})();
