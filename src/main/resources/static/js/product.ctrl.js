(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ProductCtrl', Ctrl);

    function Ctrl($http, $routeParams, readonly) {

        var vm = this;
        vm.products = [];
        vm.barcode = '';
        vm.name = '';
        vm.basePrice = '';
        vm.description = '';
        vm.propertyName = 'name';
        vm.readonly = readonly;

        vm.addNewProduct = addNewProduct;
        vm.deleteProduct = deleteProduct;

        init();

        function init() {
            //if product barcode is defined
            if ($routeParams.id !== undefined) {
                $http.get('http://localhost:8080/product/' + $routeParams.id).then(function (result) {
                    //console.log('aa: ' + result.data.securityNr);
                    vm.barcode = result.data.barcode;
                    vm.name = result.data.name;
                    vm.basePrice = result.data.basePrice;
                    vm.description = result.data.description;
                });
            }
            // if product barcode is not defined
            $http.get('http://localhost:8080/products').then(function (result) {
                //console.log('aa: ' + JSON.stringify(result));
                vm.products = result.data;
            });
        }


        function addNewProduct() {
            var newProduct = {
                    barcode: vm.barcode,
                    name: vm.name,
                    basePrice: vm.basePrice,
                    description: vm.description,
                    releaseDate: new Date()
                }
                ;
            $http.post('http://localhost:8080/products', newProduct).then(function () {
                init();
            });
        }

        function deleteProduct(barcode) {
            $http.delete('http://localhost:8080/products/' + barcode).then(function () {
                init();
            });
        }

    }

})();
