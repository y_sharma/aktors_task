(function () {
    'use strict';

    angular.module('app').config(Conf);

    function Conf($routeProvider) {

        $routeProvider.when('/order', {
            templateUrl: 'order.html',
            controller: 'OrderCtrl',
            controllerAs: 'vm',
            resolve: {
                // readonly is used to disable some inputs when editing
                readonly: function () {
                    return false;
                }
            }
        }).when('/client', {
            templateUrl: 'client.html',
            controller: 'ClientCtrl',
            controllerAs: 'vm',
            resolve: {
                readonly: function () {
                    return false;
                }
            }
        }).when('/product', {
            templateUrl: 'product.html',
            controller: 'ProductCtrl',
            controllerAs: 'vm',
            resolve: {
                readonly: function () {
                    return false;
                }
            }
        }).when('/editclient/:id', {
            templateUrl: 'client.html',
            controller: 'ClientCtrl',
            controllerAs: 'vm',
            resolve: {
                readonly: function () {
                    return true;
                }
            }
        }).when('/editproduct/:id', {
            templateUrl: 'product.html',
            controller: 'ProductCtrl',
            controllerAs: 'vm',
            resolve: {
                readonly: function () {
                    return true;
                }
            }
        }).when('/editorder/:id', {
            templateUrl: 'order.html',
            controller: 'OrderCtrl',
            controllerAs: 'vm',
            resolve: {
                readonly: function () {
                    return true;
                }
            }
        }).otherwise('/client');

    }

})();

